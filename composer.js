{
    "name": "mogio/d8-base-kit",
    "type": "drupal-base-module",
    "description": "Base Kit Main Module",
    "homepage": "https://bitbucket.org/mogio/d8-base-kit",
    "license": "GPL-2.0+",
    "require": {
    "drupal/core": "~8.3"
    }
}
