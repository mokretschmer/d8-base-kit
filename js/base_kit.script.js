(function ($, Drupal) {

    'use strict';


    Drupal.behaviors.bootstrap_barrio_subtheme = {
        attach: function (context, settings) {

            //check for inline form errors
            $('.alert-dismissible').once('inline_alert_processed').each(function () {

                var title = "";
                var message_html = "";

                //check exception kint // dpm
                if ($('.kint, .backtrace', this).length == 0) {


                    if($('ul', this).prop('outerHTML') != ""){
                        message_html = $('ul', this).prop('outerHTML');
                    }else{
                        message_html = "<div></div>";
                    }

                    //check for state
                    if ($(this).hasClass('alert-danger')) {
                        title = Drupal.t('_TR_bootbox_title_form_inline_error');
                    } else if ($(this).hasClass('alert-success')) {
                        title = Drupal.t('_TR_bootbox_title_success');
                    }


                    bootbox.alert({
                        title: title,
                        message: message_html
                    });

                    $(this).remove();
                }


            });

            //console.log($(".modal-dialog_wrapper .modal-body").text());
            if($.trim($(".modal-dialog_wrapper .modal-body").text()) != ''){

                //clear multi error messages
                $('.modal-dialog_wrapper').each(function(){
                    var this_modal = $(this);

                    $('body').append(this_modal);


                    if($('.item-list--comma-list.item-list li', this_modal).length == 1){
                        //re add btn events

                        this_modal.modal('show');
                        $('.modal-header span, .modal-footer', $(this)).click(function(){
                            $(this_modal).modal('hide');
                            setTimeout(function(){
                                $('.modal-dialog_wrapper').remove();
                            }, 500);
                        } );
                    }

                });
            }


            context.modal_action_added = 1;

        }
    };

})(jQuery, Drupal);


